import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';

import axios from 'axios'
import VueSessionStorage from 'vue-sessionstorage'

axios.interceptors.request.use(request => {
  request.headers.common['Accept'] = 'application/json'
  request.headers.common['Content-Type'] = 'application/json'

  return request
})

Vue.use(VueSessionStorage)

// All validations for session (basic validations at change route)
router.beforeEach((to, from, next) => {
  let activeSession = router.app.$session.exists('access_token')

  if (to.path == '/login') {
    if (activeSession) {
      next({ path: '/travel' })
    } else {
      router.app.$session.clear()
      next()
    }
  } else if (!activeSession) {
    router.app.$session.clear()
    next({ path: '/login' })
  } else {
    next()
  }
})

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App),
}).$mount('#app')
