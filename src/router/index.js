import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const Login = () => import(`@/components/Login.vue` /* webpackChunkName: "login" */)
const Travel = () => import(`@/components/Travel.vue` /* webpackChunkName: "travel" */)

export default new VueRouter({
  routes: [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/travel',
        name: 'travel',
        component: Travel
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
})