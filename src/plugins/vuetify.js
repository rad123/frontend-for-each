import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

// Translation provided by Vuetify (javascript)
import en from 'vuetify/es5/locale/en'
import es from 'vuetify/es5/locale/es'

export default new Vuetify({
  lang: {
    locales: { en, es },
    current: 'es',
  },
});
