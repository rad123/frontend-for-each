# forEach Frontend

## Configuración del proyecto en entorno local.
```
1. Instalar Node js con npm en la versión más reciente.
Seguidamente un link para proceso de instalación en Windows:
https://www.ecodeup.com/como-instalar-node-js-y-su-gestor-de-paquetes-npm-en-windows/

2. Instalar todas las dependencias del proyecto mediante el siguiente comando en la carpeta raíz:
npm install

3. Iniciar el proyecto en el puerto 8080 con el siguiente comando:
npm run serve

NOTA IMPORTANTE: Para el correcto funcionamiento del Frontend, se debe iniciar la API en el puerto 8000.
```

### Observaciones generales.
```
1. Se integró el proceso de login con un template de estructura sencilla.
2. Se integró el funcionamiento de las variables de sesión con estructura sencilla.
3. Se incorporó el módulo de viajes con el correspondiente listado de los mismos.
4. Se habilitó la funcionalidad de cierre de sesión.
5. Quedó pendiente la gestión de registro de viajes desde el Frontend. Sin embargo, se puede realizar
consumiendo directamente los servicios de API correspondientes.
```

### Credenciales de acceso al Sistema.
```
1. Usuario: admin
2. Contraseña: secret
```
